﻿using System;
using System.Collections.Generic;
using System.Text;
using TestWebApi.DataLayer.Result;

namespace TestWebApi.DataLayer
{
    /// <summary>
    /// Response container with data list and result
    /// </summary>
    /// <typeparam name="T">Type of enitites stored in the container's list</typeparam>
    public class DataList<T> where T : class
    {
        /// <summary>
        /// List of stored data
        /// </summary>
        public IEnumerable<T> List { get; set; }

        /// <summary>
        /// Data handling result
        /// </summary>
        public ResultMessage Result { get; set; }
    }
}
