﻿using System;

namespace TestWebApi.DataLayer.DataObjects
{
    /// <summary>
    /// Description of a company
    /// </summary>
    public class CompanyObject : DataEntity
    {
        /// <summary>
        /// Name of a company
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Exchange name trading company's stock
        /// </summary>
        public String Exchange { get; set; }

        /// <summary>
        /// Company's stock ticker on exchange
        /// </summary>
        public String StockTicker { get; set; }

        /// <summary>
        /// Company's ISIN. It has to start with 2 letters followed by numbers and letters
        /// </summary>
        public String Isin { get; set; }

        /// <summary>
        /// Company web-site address. Optional
        /// </summary>
        public String WebSite { get; set; }
    }
}
