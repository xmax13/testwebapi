﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWebApi.DataLayer.DataObjects
{
    /// <summary>
    /// Common class for data entities handling within this API
    /// </summary>
    public abstract class DataEntity
    {
        /// <summary>
        /// Entity Id
        /// </summary>
        public Guid Id { get; set; }
    }
}
