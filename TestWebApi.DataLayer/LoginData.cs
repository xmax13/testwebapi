﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestWebApi.DataLayer
{
    /// <summary>
    /// Login/password pair
    /// </summary>
    public class LoginData
    {
        /// <summary>
        /// User name. Hardcoded as 'admin'
        /// </summary>
        [Required]
        public String Login { get; set; }

        /// <summary>
        /// User password. Hardcoded as 'password'
        /// </summary>
        [Required]
        public String Password { get; set; }
    }
}
