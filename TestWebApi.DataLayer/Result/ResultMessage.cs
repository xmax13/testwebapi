﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWebApi.DataLayer.Result
{
    /// <summary>
    /// Stores data about handling results
    /// </summary>
    public class ResultMessage
    {
        /// <summary>
        /// Text of message
        /// </summary>
        public String Message { get; private set; }

        /// <summary>
        /// Message code indicating a type of occured error
        /// </summary>
        public MessageCodes MessageCode { get; private set; }

        /// <summary>
        /// Exception trace.
        /// It is used for errors occured when any exception is thrown
        /// </summary>
        public String ExceptionTrace { get; private set; }

        /// <summary>
        /// Indicates successful handling result
        /// </summary>
        public Boolean Success
        {
            get => MessageCode == MessageCodes.Success;
        }

        /// <summary>
        /// Creates a result message when successful handling result
        /// </summary>
        /// <param name="message">Text of message</param>
        public ResultMessage(String message)
        {
            MessageCode = MessageCodes.Success;
            Message = message;
        }

        /// <summary>
        /// Creates a result message when some error happened
        /// </summary>
        /// <param name="message">Text of error message</param>
        /// <param name="code">Error code</param>
        public ResultMessage(String message, MessageCodes code)
        {
            MessageCode = code;
            Message = message;
            ExceptionTrace = null;
        }

        /// <summary>
        /// Creates a result message when some exception happened
        /// </summary>
        /// <param name="exception">Exception trace, if presented</param>
        public ResultMessage(Exception exception)
        {
            // Dig the deepest exception to see the real reason of exception
            Exception exResult = exception;
            while (exResult.InnerException != null)
                exResult = exResult.InnerException;
            MessageCode = MessageCodes.InternalError;
            Message = exResult.Message;
            ExceptionTrace = exception.StackTrace;
        }
    }
}
