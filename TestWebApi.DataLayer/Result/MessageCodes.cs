﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWebApi.DataLayer.Result
{
    /// <summary>
    /// Result message codes
    /// </summary>
    public enum MessageCodes : int
    {
        /// <summary>
        /// Data handling finished successfully
        /// </summary>
        Success = 0,

        /// <summary>
        /// Internal error occured while data handling. Message has to have a exception trace
        /// </summary>
        InternalError = 1,

        /// <summary>
        /// Data storage is not ready
        /// </summary>
        StorageNotReady = 2,

        /// <summary>
        /// Company name is not set
        /// </summary>
        CompanyNameNotSet = 3,

        /// <summary>
        /// Company ISIN is not set
        /// </summary>
        CompanyIsinNotSet = 4,

        /// <summary>
        /// Company exchange name is not set
        /// </summary>
        CompanyExchangeNotSet = 5,

        /// <summary>
        /// Company ticker is not set
        /// </summary>
        CompanyTickerNotSet = 6,

        /// <summary>
        /// company is not found
        /// </summary>
        CompanyNotFound = 7,

        /// <summary>
        /// Company with specified ISIN already exists
        /// </summary>
        CompanyIsinExists = 8,

        /// <summary>
        /// Company ISIN is not valid
        /// </summary>
        CompanyIsinInvalid = 9
    }
}
