﻿using Microsoft.EntityFrameworkCore;
using System;
using TestWebApi.DataProvider.DbObjects;

namespace TestWebApi.SqliteProvider
{
    /// <summary>
    /// Db context to access data from Sqlite database
    /// </summary>
    public class SqliteContext : DbContext
    {
        public SqliteContext() : base() { }

        public SqliteContext(DbContextOptions<SqliteContext> options) : base(options) { }

        private DbSet<DbCompany> _companies;
    }
}
