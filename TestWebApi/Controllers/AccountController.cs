﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestWebApi.DataLayer;
using TestWebApi.DataProvider.DbObjects;
using TestWebApi.Security;

namespace TestWebApi.Controllers
{
    [Route("api/account/")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ITokenService _tokenService = null;

        public AccountController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        /// <summary>
        /// Authenticates user
        /// </summary>
        /// <param name="login">Login/Password pair</param>
        /// <returns>JSON Web Token</returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginData login)
        {
            if (login.Login.ToLower() == "admin" && login.Password.ToLower() == "password")
            {
                String email = $"{login.Login}@test.com";
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, email),
                    new Claim(ClaimTypes.Name, email)
                };

                var token = _tokenService.GenerateAccessToken(claims);
                var newRefreshToken = _tokenService.GenerateRefreshToken();

                return new ObjectResult(new
                {
                    authenticationToken = token,
                    refreshToken = newRefreshToken
                });
            }
            return BadRequest("Could not create token");
        }
    }
}