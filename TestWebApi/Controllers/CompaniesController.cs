﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestWebApi.DataLayer;
using TestWebApi.DataLayer.DataObjects;
using TestWebApi.DataLayer.Result;
using TestWebApi.Repository;

namespace TestWebApi.Controllers
{
    /// <summary>
    /// Companies controller
    /// </summary>
    [Route("api/companies")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private ICompanyRepository _repository = null;

        /// <summary>
        /// Companies controller
        /// </summary>
        /// <param name="repository">Companies data repository</param>
        public CompaniesController(ICompanyRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get full list of companies from a storage
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        /// Usage GET /companies/list
        /// </remarks>
        /// <returns>
        /// Returns a container with a data list and handling result
        /// </returns>
        /// <response code="200">Success. List is not null</response>
        /// <response code="400">Result contains error data. List is null</response>
        [HttpGet("list")]
        [ProducesResponseType(typeof(DataList<CompanyObject>), 200)]
        [ProducesResponseType(typeof(DataList<CompanyObject>), 400)]
        public async Task<IActionResult> GetList()
        {
            Console.WriteLine("Companies/GetList");
            JsonResult result = null;
            DataList<CompanyObject> dataResult = null;
            try
            {
                dataResult = await _repository.GetCompanies();
                result = new JsonResult(dataResult);
                if (dataResult.Result.Success)
                    result.StatusCode = 200;
                else
                    result.StatusCode = 400;
            }
            catch (Exception ex)
            {
                dataResult = new DataList<CompanyObject>();
                dataResult.Result = new ResultMessage(ex);
                result = new JsonResult(dataResult);
                result.StatusCode = 400;
            }
            return result;
        }

        /// <summary>
        /// Get a company by its identifier. Method requires authentication
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        /// Usage GET /companies/item/isin/0BE2CF3E-E374-4A98-9F23-EF8E9EE892C2
        /// </remarks>
        /// <param name="id">Identifier of the company to be retrieved. Identifier is GUID</param>
        /// <returns>
        /// Returns a container with a data list and handling result. 
        /// If success and the item is found, the data list contains one item
        /// </returns>
        /// <response code="200">Success. List is not null.
        /// It contains either one found item, or it is empty, if nothing was found</response>
        /// <response code="400">Result contains error data. List is null</response>
        [Authorize]
        [HttpGet("item/id/{id}")]
        [ProducesResponseType(typeof(DataList<CompanyObject>), 200)]
        [ProducesResponseType(typeof(DataList<CompanyObject>), 400)]
        public async Task<IActionResult> GetItemById(Guid id)
        {
            Console.WriteLine("Companies/GetCompany by Id: {0}", id);
            JsonResult result = null;
            DataList<CompanyObject> dataResult = null;
            try
            {
                dataResult = await _repository.GetCompany(id);
                // If no errors occured while receiving data from storage
                if (dataResult.Result.Success)
                {
                    // If the data list is null or empty
                    if ((dataResult.List != null) ? dataResult.List.Any() == false : true)
                    {
                        // Prepare null list and message
                        dataResult.List = null;
                        dataResult.Result = new ResultMessage(
                            "Company is not found. It is likely removed.",
                            MessageCodes.CompanyNotFound);
                    }
                }
                result = new JsonResult(dataResult);
                if (dataResult.Result.Success)
                    result.StatusCode = 200;
                else
                    result.StatusCode = 400;
            }
            catch (Exception ex)
            {
                dataResult = new DataList<CompanyObject>();
                dataResult.Result = new ResultMessage(ex);
                result = new JsonResult(dataResult);
                result.StatusCode = 400;
            }
            return result;
        }

        /// <summary>
        /// Get a company by its ISIN
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        /// Usage GET /companies/item/isin/DE000PAH0038
        /// </remarks>
        /// <param name="isin">ISIN of the company to be retrieved</param>
        /// <returns>
        /// Returns a container with a data list and handling result. 
        /// If success and the item is found, the data list contains one item
        /// </returns>
        /// <response code="200">Success. List is not null.
        /// It contains either one found item, or it is empty, if nothing was found</response>
        /// <response code="400">Result contains error data. List is null</response>
        [HttpGet("item/isin/{isin}")]
        [ProducesResponseType(typeof(DataList<CompanyObject>), 200)]
        [ProducesResponseType(typeof(DataList<CompanyObject>), 400)]
        public async Task<IActionResult> GetItemByIsin(String isin)
        {
            Console.WriteLine("Companies/GetCompany by ISIN: {0}", isin);
            JsonResult result = null;
            DataList<CompanyObject> dataResult = null;
            try
            {
                dataResult = await _repository.GetCompany(isin);
                // If no errors occured while receiving data from storage
                if (dataResult.Result.Success)
                {
                    // If the data list is null or empty
                    if ((dataResult.List != null) ? dataResult.List.Any() == false : true)
                    {
                        // Prepare null list and message
                        dataResult.List = null;
                        dataResult.Result = new ResultMessage(
                            "Company with specified ISIN is not found. It is likely removed.",
                            MessageCodes.CompanyNotFound);
                    }
                }
                result = new JsonResult(dataResult);
                if (dataResult.Result.Success)
                    result.StatusCode = 200;
                else
                    result.StatusCode = 400;
            }
            catch (Exception ex)
            {
                dataResult = new DataList<CompanyObject>();
                dataResult.Result = new ResultMessage(ex);
                result = new JsonResult(dataResult);
                result.StatusCode = 400;
            }
            return result;
        }

        /// <summary>
        /// Creates a new company object in a storage. Method requires authentication
        /// </summary>
        /// <param name="company">Company object to be added to a storage</param>
        /// <returns>Returns ResultMessage object with saving result</returns>
        /// <response code="200">Returns successful ResultMessage object</response>
        /// <response code="400" >Returns ResultMessage object with error data</response>
        [Authorize]
        [HttpPost("new")]
        [ProducesResponseType(typeof(ResultMessage), 200)]
        [ProducesResponseType(typeof(ResultMessage), 400)]
        public async Task<IActionResult> CreateItem([FromBody] CompanyObject company)
        {
            Console.WriteLine("Companies/CreateCompany: {0}", 
                (company == null) ? "": ((company.Name == null) ? "<not set>" : company.Name));
            JsonResult result = null;
            ResultMessage msg = null;
            try
            {
                company.Id = Guid.Empty;
                msg = await _repository.SaveCompany(company);
                result = new JsonResult(msg);
                if (msg.Success)
                    result.StatusCode = 200;
                else
                    result.StatusCode = 400;
            }
            catch (Exception ex)
            {
                msg = new ResultMessage(ex);
                result = new JsonResult(msg);
                result.StatusCode = 400;
            }
            return result;
        }

        /// <summary>
        /// Updates data for the company with specified id. Method requires authentication
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        /// PUT /update/DC64F188-13AF-49A7-90CC-024E3D4194DF
        /// 
        /// </remarks>
        /// <param name="id">Identifier of a company</param>
        /// <param name="company">Company object with data to be updated</param>
        /// <returns>Returns ResultMessage object with updating result</returns>
        /// <response code="200">Returns successful ResultMessage object</response>
        /// <response code="400" >Returns ResultMessage object with error data</response>
        [Authorize]
        [HttpPut("update/{id}")]
        [ProducesResponseType(typeof(ResultMessage), 200)]
        [ProducesResponseType(typeof(ResultMessage), 400)]
        public async Task<IActionResult> UpdateItem(Guid id, [FromBody] CompanyObject company)
        {
            Console.WriteLine("Companies/UpdateCompany: {0} - {1}", id, 
                (company == null) ? "" : ((company.Name == null) ? "<not set>" : company.Name));
            JsonResult result = null;
            ResultMessage msg = null;
            try
            {
                msg = await _repository.SaveCompany(company);
                result = new JsonResult(msg);
                if (msg.Success)
                    result.StatusCode = 200;
                else
                    result.StatusCode = 400;
            }
            catch (Exception ex)
            {
                msg = new ResultMessage(ex);
                result = new JsonResult(msg);
                result.StatusCode = 400;
            }
            return result;
        }
    }
}
