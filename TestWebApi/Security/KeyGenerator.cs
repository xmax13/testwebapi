﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace TestWebApi.Security
{
    /// <summary>
    /// Test key generator
    /// </summary>
    public static class KeyGenerator
    {
        /// <summary>
        /// Generate a key
        /// </summary>
        /// <returns></returns>
        public static String Generate()
        {
            byte[] key = new byte[64];

            var randomGen = RandomNumberGenerator.Create();
            randomGen.GetBytes(key);

            return Convert.ToBase64String(key);
        }
    }
}
