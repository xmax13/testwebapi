﻿using CompanyWebClient.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CompanyWebClient.Models
{
    /// <summary>
    /// Company model
    /// </summary>
    public class CompanyModel
    {
        private CompanyObject _dataObject = null;

        public CompanyModel()
        {
            _dataObject = new CompanyObject();
        }

        public CompanyModel(CompanyObject data)
        {
            _dataObject = data;
            if (_dataObject != null)
            {
                if (_dataObject.Id.Equals(Guid.Empty))
                    _dataObject.Id = Guid.NewGuid();
            }
        }

        /// <summary>
        /// Internal data container
        /// </summary>
        public CompanyObject DataObject
        {
            get { return _dataObject; }
        }

        /// <summary>
        /// Company identifier
        /// </summary>
        public Guid Id
        {
            get
            {
                return (_dataObject == null) ? Guid.Empty : _dataObject.Id;
            }
            set
            {
                if (_dataObject != null)
                    _dataObject.Id = value;
            }
        }

        /// <summary>
        /// ISIN
        /// </summary>
        [Required(ErrorMessage = "ISIN is not set")]
        [StringLength(50)]
        [Display(Name = "ISIN")]
        public String Isin
        {
            get
            {
                return (_dataObject == null) ? "" : _dataObject.Isin;
            }
            set
            {
                if (_dataObject != null)
                    _dataObject.Isin = value;
            }
        }

        /// <summary>
        /// Name of a company
        /// </summary>
        [Required(ErrorMessage = "Name is not set")]
        [StringLength(100)]
        [Display(Name = "Company")]
        public String Name
        {
            get
            {
                return (_dataObject == null) ? "" : _dataObject.Name;
            }
            set
            {
                if (_dataObject != null)
                    _dataObject.Name = value;
            }
        }

        /// <summary>
        /// Exchange name
        /// </summary>
        [Required(ErrorMessage = "Exchange is not set")]
        [StringLength(50)]
        [Display(Name = "Exchange")]
        public String Exchange
        {
            get
            {
                return (_dataObject == null) ? "" : _dataObject.Exchange;
            }
            set
            {
                if (_dataObject != null)
                    _dataObject.Exchange = value;
            }
        }

        /// <summary>
        /// Stock ticker
        /// </summary>
        [Required(ErrorMessage = "Stock ticker is not set")]
        [StringLength(40)]
        [Display(Name = "Stock ticker")]
        public String StockTicker
        {
            get
            {
                return (_dataObject == null) ? "" : _dataObject.StockTicker;
            }
            set
            {
                if (_dataObject != null)
                    _dataObject.StockTicker = value;
            }
        }

        /// <summary>
        /// Company web site
        /// </summary>
        [StringLength(150)]
        [Display(Name = "Website")]
        public String WebSite
        {
            get
            {
                return (_dataObject == null) ? "" :
                    (String.IsNullOrEmpty(_dataObject.WebSite) ? "" : _dataObject.WebSite);
            }
            set
            {
                if (_dataObject != null)
                    _dataObject.WebSite = value;
            }
        }
    }
}