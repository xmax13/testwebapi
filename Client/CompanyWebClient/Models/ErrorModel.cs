﻿using CompanyWebClient.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CompanyWebClient.Models
{
    public class ErrorModel
    {
        private ResultMessage _msg = null;

        [Required]
        [Display(Name = "Error code")]
        public Int32 ErrorCode
        {
            get { return _msg.MessageCode; }
        }

        [Required]
        [Display(Name = "Message")]
        public String Message
        {
            get { return _msg.Message; }
        }

        [Required]
        [Display(Name = "Exception trace")]
        public String ExceptionTrace
        {
            get { return _msg.ExceptionTrace; }
        }

        public ErrorModel(ResultMessage msg)
        {
            _msg = msg;
        }
    }
}