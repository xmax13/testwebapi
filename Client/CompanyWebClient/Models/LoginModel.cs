﻿using CompanyWebClient.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CompanyWebClient.Models
{
    /// <summary>
    /// Company model
    /// </summary>
    public class LoginModel
    {
        private LoginData _dataObject = null;

        public LoginModel()
        {
            _dataObject = new LoginData();
        }

        /// <summary>
        /// Internal data container
        /// </summary>
        public LoginData DataObject
        {
            get { return _dataObject; }
        }

        /// <summary>
        /// User name
        /// </summary>
        [Required(ErrorMessage = "User name is not set")]
        [StringLength(50)]
        [Display(Name = "User name")]
        public String Login
        {
            get { return _dataObject.Login; }
            set { _dataObject.Login = value; }
        }

        /// <summary>
        /// Password
        /// </summary>
        [Required(ErrorMessage = "Password is not set")]
        [StringLength(50)]
        [Display(Name = "Password")]
        public String Password
        {
            get { return _dataObject.Password; }
            set { _dataObject.Password = value; }
        }
    }
}