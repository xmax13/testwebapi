﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using CompanyWebClient.DataLayer;
using Newtonsoft.Json;

namespace CompanyWebClient.Repository
{
    /// <summary>
    /// Repository for authentication on WebAPI server.
    /// This repository must be invoked as singleton in order to have 
    /// only one HttpClient instance for all requests
    /// </summary>
    public class AccountRepository : IAccountRepository
    {
        private readonly HttpClient client = null;

        private String apiAddress = "api/account/";

        /// <summary>
        /// Returns address of API server from configuration
        /// </summary>
        private String GetApiServer
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["DataServerHost"];
            }
        }

        /// <summary>
        /// Get full API address, including server address, and api, excluding controller and method name
        /// </summary>
        private String GetApiAddress
        {
            get { return $"{GetApiServer}{apiAddress}"; }
        }

        public AccountRepository()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(GetApiAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public String Authenticate(String user, String password, out String error)
        {
            error = "";
            String token = null;
            try
            {
                Dictionary<String, String> tokenDetails = null;
                var login = new LoginData()
                {
                    Login = user,
                    Password = password
                };
                var response = client.PostAsJsonAsync<LoginData>($"login", login).Result;
                if (response.IsSuccessStatusCode)
                {
                    // Get object with tokens
                    tokenDetails = JsonConvert.DeserializeObject<Dictionary<String, String>>(response.Content.ReadAsStringAsync().Result);
                    if (tokenDetails != null && tokenDetails.Any())
                    {
                        // Get authentication token. Refresh token is not used for a while
                        token = tokenDetails.FirstOrDefault().Value;
                    }
                }
                else
                    error = $"Status {response.ReasonPhrase}";
            }
            catch (Exception)
            {
                token = null;
            }
            return token;
        }
    }
}