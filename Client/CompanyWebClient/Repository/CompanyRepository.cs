﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using CompanyWebClient.DataLayer;
using Newtonsoft.Json;

namespace CompanyWebClient.Repository
{
    /// <summary>
    /// Repository for data about companies.
    /// This repository must be invoked as singleton in order to have 
    /// only one HttpClient instance for all requests
    /// </summary>
    public class CompanyRepository : ICompanyRepository
    {
        private readonly HttpClient client = null;

        private String apiAddress = "api/companies/";

        /// <summary>
        /// Returns address of API server from configuration
        /// </summary>
        private String GetApiServer
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["DataServerHost"];
            }
        }

        /// <summary>
        /// Get full API address, including server address, and api, controller name, excluding method name
        /// </summary>
        private String GetApiAddress
        {
            get { return $"{GetApiServer}{apiAddress}"; }
        }

        public CompanyRepository()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(GetApiAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Returns all companies
        /// </summary>
        /// <returns>Data cointainer and operation result</returns>
        public DataList<CompanyObject> GetCompanies()
        {
            return GetCompaniesData(null, "list/");
        }

        /// <summary>
        /// Returns a company with specified identifier
        /// </summary>
        /// <param name="token">JWT</param>
        /// <returns>Data cointainer and operation result</returns>
        public DataList<CompanyObject> GetCompany(String token, Guid id)
        {
            return GetCompaniesData(token, $"item/id/{id}");
        }

        /// <summary>
        /// Returns a company with specified ISIN
        /// </summary>
        /// <returns>Data cointainer and operation result</returns>
        public DataList<CompanyObject> GetCompany(string isin)
        {
            return GetCompaniesData(null, $"item/isin/{isin}");
        }

        /// <summary>
        /// Common method for all getting operations.
        /// Retrieves companies from a storage
        /// </summary>
        /// <param name="request">API request</param>
        /// <returns>Data cointainer and operation result</returns>
        private DataList<CompanyObject> GetCompaniesData(String token, String request)
        {
            DataList<CompanyObject> dataResult = null;
            try
            {
                if (!String.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue();
                    client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", token);
                }
                var responseTask = client.GetAsync(request);
                responseTask.Wait();

                var result = responseTask.Result;
                var readTask = result.Content.ReadAsAsync<DataList<CompanyObject>>();
                readTask.Wait();
                dataResult = readTask.Result;
                if (dataResult == null)
                {
                    if (result.IsSuccessStatusCode)
                        throw new HttpRequestException("Http request error.");
                    else
                    {
                        String errorMsg = String.Format("Method '{0} {1}' incured error '{2}'",
                            result.RequestMessage.Method,
                            result.RequestMessage.RequestUri,
                            result.StatusCode.ToString());
                        throw new HttpRequestException(errorMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                dataResult = new DataList<CompanyObject>();
                dataResult.List = new List<CompanyObject>();
                dataResult.Result = ResultMessage.NewException(ex);
            }
            return dataResult;
        }

        /// <summary>
        /// Save company data to a storage.
        /// New company must have empty id
        /// </summary>
        /// <param name="token">JWT</param>
        /// <param name="company">Object containing data about company to be saved</param>
        /// <returns>Operation result</returns>
        public ResultMessage SaveCompany(String token, CompanyObject company)
        {
            ResultMessage msg = new ResultMessage();
            try
            {
                String request = "";
                Task<HttpResponseMessage> responseTask;
                if (company.Id.Equals(Guid.Empty))
                {
                    // Create new
                    request = "new";
                    responseTask = client.PostAsJsonAsync<CompanyObject>(request, company);
                }
                else
                {
                    // Modify existing
                    request = $"update/{company.Id}";
                    responseTask = client.PutAsJsonAsync<CompanyObject>(request, company);
                }
                responseTask.Wait();

                var result = responseTask.Result;
                var readTask = result.Content.ReadAsAsync<ResultMessage>();
                readTask.Wait();
                msg = readTask.Result;
                Boolean msgFlag = (msg == null);
                if (msg != null)
                    msgFlag = msg.Success;
                if (!result.IsSuccessStatusCode && msgFlag)
                {
                    // it means that there is error on network level and default 
                    //  ResultMessage received
                    msg = new ResultMessage();
                    msg.Success = false;
                    msg.Message = String.Format("Method '{0} {1}' incured error '{2}'",
                        result.RequestMessage.Method,
                        result.RequestMessage.RequestUri,
                        result.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                msg = ResultMessage.NewException(ex);
            }
            return msg;
        }
    }
}