﻿using CompanyWebClient.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyWebClient.Repository
{
    public interface IAccountRepository
    {
        String Authenticate(String user, String password, out String error);
    }
}