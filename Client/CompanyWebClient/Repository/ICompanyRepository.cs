﻿using CompanyWebClient.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyWebClient.Repository
{
    public interface ICompanyRepository
    {
        DataList<CompanyObject> GetCompanies();
        DataList<CompanyObject> GetCompany(String token, Guid id);
        DataList<CompanyObject> GetCompany(String isin);
        ResultMessage SaveCompany(String token, CompanyObject company);
    }
}