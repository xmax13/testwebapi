﻿using CompanyWebClient.DataLayer;
using CompanyWebClient.Models;
using CompanyWebClient.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CompanyWebClient.Controllers
{
    public class AccountController : Controller
    {
        private IAccountRepository repo = null;

        public AccountController(IAccountRepository dataRepository)
        {
            repo = dataRepository;
        }

        public ActionResult Authenticate()
        {
            Session["auth_error"] = null;
            String token = Session["token"] as String;
            if (String.IsNullOrEmpty(token))
            {
                // New authentication
                return View("Login", new LoginModel());
            }
            else
            {
                // Log off
                Session["token"] = null;
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Authenticate(LoginModel loginData)
        {
            if (ModelState.IsValid)
            {
                Session["auth_error"] = null;
                String error;
                String token = repo.Authenticate(
                    loginData.Login,
                    loginData.Password,
                    out error);
                if (String.IsNullOrEmpty(token))
                {
                    Session["auth_error"] = $"Authentication error. {error}";
                }
                else
                    Session["token"] = token;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("Login", loginData);
            }
        }
    }
}