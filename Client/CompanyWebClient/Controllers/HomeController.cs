﻿using CompanyWebClient.DataLayer;
using CompanyWebClient.Models;
using CompanyWebClient.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CompanyWebClient.Controllers
{
    public class HomeController : Controller
    {
        private ICompanyRepository repo = null;

        public HomeController(ICompanyRepository dataRepository)
        {
            repo = dataRepository;
        }

        public ActionResult Index()
        {
            ViewBag.Search = false;
            List<CompanyModel> modelList = new List<CompanyModel>();
            String token = Session["token"] as String;
            ViewBag.Auth = (!String.IsNullOrEmpty(token));
            ViewBag.LoginButton = (ViewBag.Auth) ? "Log off" : "Log in";
            String authError = Session["auth_error"] as String;
            if (authError == null)
            {
                DataList<CompanyObject> dataList = repo.GetCompanies();
                modelList.AddRange(dataList.List.Select(x => new CompanyModel(x)));
                if (!dataList.Result.Success)
                    ViewBag.ErrorMessage = new ErrorModel(dataList.Result);
            }
            else
            {
                ResultMessage msg = new ResultMessage();
                msg.Message = authError;
                ViewBag.ErrorMessage = new ErrorModel(msg);
            }
            return View("Index", modelList);

        }

        public ActionResult Details(Guid? id)
        {
            String token = Session["token"] as String;
            ViewBag.Auth = (!String.IsNullOrEmpty(token));
            ViewBag.LoginButton = (ViewBag.Auth) ? "Log off" : "Log in";
            CompanyModel company = null;
            Guid companyId = (id == null) ? Guid.Empty : id.Value;
            Boolean createNew = false;
            if (companyId.Equals(Guid.Empty))
                createNew = true;
            if (createNew)
            {
                // Create a new company
                ViewBag.Title = "Create a new company";
                ViewBag.SubmitTitle = "Create";
                company = new CompanyModel();
            }
            else 
            {
                // Modify existing company
                ViewBag.Title = "Modify company details";
                ViewBag.SubmitTitle = "Update";
                DataList<CompanyObject> dataList = repo.GetCompany(token, id.Value);
                if (dataList.Result.Success)
                {
                    CompanyObject companyObject = dataList.List[0];
                    if (companyObject.Id.Equals(Guid.Empty))
                        return Details((Guid?)null);
                    else
                        company = new CompanyModel(companyObject);
                }
                else
                {
                    ViewBag.ErrorMessage = new ErrorModel(dataList.Result);
                    company = new CompanyModel(null);
                }
            }
            return View("Details", company);
        }

        [HttpPost]
        public ActionResult Details(CompanyModel company)
        {
            if (ModelState.IsValid)
            {
                String token = Session["token"] as String;
                ResultMessage result = repo.SaveCompany(token, company.DataObject);
                if (result.Success)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    Guid? id = company.Id;
                    if (company.Id.Equals(Guid.Empty))
                        id = null;
                    ViewBag.ErrorMessage = new ErrorModel(result);
                    return Details(id);
                }
            }
            else
            {
                Guid? id = company.Id;
                if (company.Id.Equals(Guid.Empty))
                    id = null;
                return Details(id);
            }
        }

        [HttpPost]
        public ActionResult Search(String searchBox)
        {
            String token = Session["token"] as String;
            ViewBag.Auth = (!String.IsNullOrEmpty(token));
            ViewBag.LoginButton = (ViewBag.Auth) ? "Log off" : "Log in";
            DataList<CompanyObject> dataList = repo.GetCompany(searchBox);
            List<CompanyModel> modelList = new List<CompanyModel>();
            ViewBag.Search = true;
            if (dataList.Result.Success)
            {
                modelList.AddRange(dataList.List.Select(x => new CompanyModel(x)));
            }
            else
            {
                if (dataList.Result.MessageCode != 7)  // not found by ISIN
                    ViewBag.ErrorMessage = new ErrorModel(dataList.Result);
            }
            return View("Index", modelList);
        }
    }
}