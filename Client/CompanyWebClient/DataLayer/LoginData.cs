﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CompanyWebClient.DataLayer
{
    /// <summary>
    /// Authentication data
    /// </summary>
    public class LoginData
    {
        /// <summary>
        /// User login
        /// </summary>
        [Required]
        public String Login { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [Required]
        public String Password { get; set; }
    }
}