﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyWebClient.DataLayer
{
    /// <summary>
    /// Data container received from server
    /// </summary>
    /// <typeparam name="T">Type of data object</typeparam>
    public class DataList<T>
    {
        /// <summary>
        /// Data list
        /// </summary>
        public List<T> List { get; set; }

        /// <summary>
        /// Data handling result
        /// </summary>
        public ResultMessage Result { get; set; }
    }
}