﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyWebClient.DataLayer
{
    /// <summary>
    /// Requst result
    /// </summary>
    public class ResultMessage
    {
        /// <summary>
        /// Message text
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        /// Message code. 0 - success
        /// </summary>
        public Int32 MessageCode { get; set; }

        /// <summary>
        /// Stack trace if an exception occured
        /// </summary>
        public String ExceptionTrace { get; set; }

        /// <summary>
        /// Flag of successful result
        /// </summary>
        public Boolean Success { get; set; }

        public static ResultMessage NewException(Exception ex)
        {
            ResultMessage result = new ResultMessage();
            result.Success = false;
            Exception target = ex;
            while (target.InnerException != null)
                target = target.InnerException;
            result.Message = target.Message;
            result.ExceptionTrace = target.StackTrace;
            return result;
        }
    }
}