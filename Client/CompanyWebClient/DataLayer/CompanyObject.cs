﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyWebClient.DataLayer
{
    public class CompanyObject
    {
        /// <summary>
        /// Company identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ISIN
        /// </summary>
        public String Isin { get; set; }

        /// <summary>
        /// Name of a company
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Exchange name
        /// </summary>
        public String Exchange { get; set; }

        /// <summary>
        /// Stock ticker
        /// </summary>
        public String StockTicker { get; set; }

        /// <summary>
        /// Company web site
        /// </summary>
        public String WebSite { get; set; }
    }
}