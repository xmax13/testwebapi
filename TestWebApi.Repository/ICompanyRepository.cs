﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestWebApi.DataLayer;
using TestWebApi.DataLayer.DataObjects;
using TestWebApi.DataLayer.Result;

namespace TestWebApi.Repository
{
    /// <summary>
    /// Company repository
    /// </summary>
    public interface ICompanyRepository
    {
        /// <summary>
        /// Get a full list of companies
        /// </summary>
        /// <returns>Container with a list of found companies and operation result</returns>
        Task<DataList<CompanyObject>> GetCompanies();

        /// <summary>
        /// Get a company by its identifier
        /// </summary>
        /// <param name="id">Identifier of a company in a storage</param>
        /// <returns>Container with found company and operation result</returns>
        Task<DataList<CompanyObject>> GetCompany(Guid id);

        /// <summary>
        /// Get a company by its ISIN
        /// </summary>
        /// <param name="isin">ISIN of a company to be found</param>
        /// <returns>Container with found company and operation result</returns>
        Task<DataList<CompanyObject>> GetCompany(String isin);

        /// <summary>
        /// Save a company object
        /// </summary>
        /// <param name="company">Company object</param>
        /// <returns>Operation result</returns>
        Task<ResultMessage> SaveCompany(CompanyObject company);
    }
}
