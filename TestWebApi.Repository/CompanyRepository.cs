﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestWebApi.DataLayer;
using TestWebApi.DataLayer.DataObjects;
using TestWebApi.DataLayer.Result;
using TestWebApi.DataProvider;
using TestWebApi.DataProvider.DataBuilders;
using TestWebApi.DataProvider.DbObjects;

namespace TestWebApi.Repository
{
    /// <summary>
    /// Repository providing companies data from a storage
    /// </summary>
    public class CompanyRepository : ICompanyRepository
    {
        /// <summary>
        /// DbContext to access data
        /// </summary>
        private DbContext _context = null;

        public CompanyRepository(DbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a full list of companies
        /// </summary>
        /// <returns>Container with a list of found companies and operation result</returns>
        public async Task<DataList<CompanyObject>> GetCompanies()
        {
            // Default result
            DataList<CompanyObject> dataList = new DataList<CompanyObject>();
            dataList.Result = new ResultMessage("");
            if (_context == null)
            {
                dataList.Result = new ResultMessage(
                    "Storage is not ready. Make sure database is available and configured properly.",
                    MessageCodes.StorageNotReady);
                return await Task.FromResult(dataList);
            }
            try
            {
                // Get ordered list of companies containing DTO
                dataList.List = _context.Set<DbCompany>()
                    .AsNoTracking()
                    .OrderBy(x => x.Name)
                    .Select(x => CompanyBuilder.Build(x));
            }
            catch (Exception ex)
            {
                dataList.Result = new ResultMessage(ex);
            }
            return await Task.FromResult(dataList);
        }

        /// <summary>
        /// Get a company by its identifier
        /// </summary>
        /// <param name="id">Identifier of a company in a storage</param>
        /// <returns>Container with found company and operation result</returns>
        public async Task<DataList<CompanyObject>> GetCompany(Guid id)
        {
            // Default result
            DataList<CompanyObject> dataList = new DataList<CompanyObject>();
            dataList.Result = new ResultMessage("");
            if (_context == null)
            {
                dataList.Result = new ResultMessage(
                    "Storage is not ready. Make sure database is available and configured properly.",
                    MessageCodes.StorageNotReady);
                return await Task.FromResult(dataList);
            }
            try
            {
                // Get found company as DTO
                DbCompany company = await _context.Set<DbCompany>()
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Id == id);
                dataList.List = new List<CompanyObject>();
                if (company != null)
                    ((List<CompanyObject>)dataList.List).Add(CompanyBuilder.Build(company));
            }
            catch (Exception ex)
            {
                dataList.Result = new ResultMessage(ex);
            }
            return await Task.FromResult(dataList);
        }

        /// <summary>
        /// Get a company by its ISIN
        /// </summary>
        /// <param name="isin">ISIN of a company to be found</param>
        /// <returns>Container with found company and operation result</returns>
        public async Task<DataList<CompanyObject>> GetCompany(String isin)
        {
            // Default result
            DataList<CompanyObject> dataList = new DataList<CompanyObject>();
            dataList.Result = new ResultMessage("");
            if (_context == null)
            {
                dataList.Result = new ResultMessage(
                    "Storage is not ready. Make sure database is available and configured properly.",
                    MessageCodes.StorageNotReady);
                return await Task.FromResult(dataList);
            }
            try
            {
                // Get found company as DTO
                isin = isin.ToUpper();
                DbCompany company = await _context.Set<DbCompany>()
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Isin == isin);
                dataList.List = new List<CompanyObject>();
                if (company != null)
                    ((List<CompanyObject>)dataList.List).Add(CompanyBuilder.Build(company));
            }
            catch (Exception ex)
            {
                dataList.Result = new ResultMessage(ex);
            }
            return await Task.FromResult(dataList);
        }

        /// <summary>
        /// Save a company object
        /// </summary>
        /// <param name="company">Company object to be saved in a storage</param>
        /// <returns>Operation result</returns>
        public async Task<ResultMessage> SaveCompany(CompanyObject company)
        {
            ResultMessage result = new ResultMessage("");
            if (_context == null)
                result = new ResultMessage(
                    "Storage is not ready. Make sure database is available and configured properly.", 
                    MessageCodes.StorageNotReady);
            // Data validation
            if (company == null)
                result = new ResultMessage("Company data is empty.", MessageCodes.CompanyNameNotSet);
            else
            {
                if (!String.IsNullOrEmpty(company.Isin))
                    company.Isin = company.Isin.ToUpper();
                if (String.IsNullOrEmpty(company.Name))
                    result = new ResultMessage("Company name is empty. Set the name", 
                        MessageCodes.CompanyNameNotSet);
                else if (String.IsNullOrEmpty(company.Isin))
                    result = new ResultMessage("Company ISIN is empty. Set the ISIN",
                        MessageCodes.CompanyIsinNotSet);
                else if (String.IsNullOrEmpty(company.Exchange))
                    result = new ResultMessage("Company exchange name is empty. Set the name",
                        MessageCodes.CompanyExchangeNotSet);
                else if (String.IsNullOrEmpty(company.StockTicker))
                    result = new ResultMessage("Company stock ticker is empty. Set the ticker",
                        MessageCodes.CompanyTickerNotSet);
                else if (IsinValidator.Validate(company.Isin) == false)
                    result = new ResultMessage(
                        "Company ISIN is not valid. It has to start with two letters followed by numbers and letters",
                        MessageCodes.CompanyIsinInvalid);
            }
            if (!result.Success)
                return await Task.FromResult(result);
            // Save data
            try
            {
                // Check if a company with the same ISIN and different Id exists
                DbCompany test = await _context.Set<DbCompany>()
                    .FirstOrDefaultAsync(x => (!x.Id.Equals(company.Id) && x.Isin == company.Isin));
                if (test == null)
                {
                    DbCompany dbCompany = null;
                    // Id check
                    if (company.Id.Equals(Guid.Empty))
                    {
                        // Create new
                        company.Id = Guid.NewGuid();
                        dbCompany = CompanyBuilder.Build(company);
                        _context.Set<DbCompany>().Add(dbCompany);
                    }
                    else
                    {
                        // Update existing
                        dbCompany = CompanyBuilder.Build(company);
                        _context.Set<DbCompany>().Update(dbCompany);
                    }
                    _context.SaveChanges();
                    _context.Entry<DbCompany>(dbCompany).State = EntityState.Detached;
                }
                else
                {
                    result = new ResultMessage("Company with the same ISIN already exists",
                        MessageCodes.CompanyIsinExists);
                }
            }
            catch (Exception ex)
            {
                result = new ResultMessage(ex);
            }
            return await Task.FromResult(result);
        }
    }
}