﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestWebApi.DataProvider.DbObjects;

namespace TestWebApi.DataProvider
{
    public class DataContext : DbContext
    {
        public DbSet<DbCompany> Companies { get; set; }

        public DataContext() : base() { }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
    }
}
