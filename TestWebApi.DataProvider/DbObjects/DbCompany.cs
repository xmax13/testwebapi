﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestWebApi.DataProvider.DbObjects
{
    /// <summary>
    /// Company object
    /// </summary>
    public class DbCompany
    {
        /// <summary>
        /// Company identifier
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Company ISIN
        /// </summary>
        [Required]
        [StringLength(50)]
        public String Isin { get; set; }

        /// <summary>
        /// Company name
        /// </summary>
        [Required]
        [StringLength(100)]
        public String Name { get; set; }

        /// <summary>
        /// Exchange name
        /// </summary>
        [Required]
        [StringLength(50)]
        public String Exchange { get; set; }

        /// <summary>
        /// Company ticker
        /// </summary>
        [Required]
        [StringLength(40)]
        public String Ticker { get; set; }

        /// <summary>
        /// Company name
        /// </summary>
        [StringLength(150)]
        public String WebSite { get; set; }
    }
}
