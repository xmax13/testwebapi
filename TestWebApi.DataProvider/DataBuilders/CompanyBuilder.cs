﻿using System;
using System.Collections.Generic;
using System.Text;
using TestWebApi.DataLayer.DataObjects;
using TestWebApi.DataProvider.DbObjects;

namespace TestWebApi.DataProvider.DataBuilders
{
    public static class CompanyBuilder
    {
        public static DbCompany Build(CompanyObject company)
        {
            DbCompany db = new DbCompany()
            {
                Id = company.Id,
                Name = company.Name,
                Isin = company.Isin.ToUpper(),
                Ticker = company.StockTicker,
                Exchange = company.Exchange,
                WebSite = company.WebSite
            };
            return db;
        }

        public static CompanyObject Build(DbCompany db)
        {
            CompanyObject company = new CompanyObject()
            {
                Id = db.Id,
                Name = db.Name,
                Isin = db.Isin,
                StockTicker = db.Ticker,
                Exchange = db.Exchange,
                WebSite = db.WebSite
            };
            return company;
        }
    }
}
