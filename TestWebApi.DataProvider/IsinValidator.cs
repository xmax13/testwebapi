﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace TestWebApi.DataProvider
{
    /// <summary>
    /// Validator for ISIN
    /// </summary>
    public static class IsinValidator
    {
        /// <summary>
        /// Validate passed ISIN
        /// </summary>
        /// <param name="isin">ISIN to be validated</param>
        /// <returns>true, if ISIN is valid</returns>
        public static Boolean Validate(String isin)
        {
            Regex rx = new Regex(@"\A[A-Z]{2}\w", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            MatchCollection matches = rx.Matches(isin);
            return matches.Count > 0;
        }
    }
}
