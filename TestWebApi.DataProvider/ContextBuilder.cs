﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestWebApi.DataProvider
{
    /// <summary>
    /// DbContext builder
    /// </summary>
    public static class ContextBuilder
    {
        /// <summary>
        /// Builds DbContext for specified type of data storage
        /// </summary>
        /// <remarks>
        /// Supported types:
        /// sqlite: Sqlite
        /// fake: In-memory database for tests
        /// </remarks>
        /// <param name="contextType">Type of storage (fake, sqlite, etc)</param>
        /// <param name="contextParams">Parameters of storage, e.g. connection string, database name, etc</param>
        /// <returns>DbContext instance</returns>
        public static DbContext Build(String contextType, String contextParams)
        {
            DbContext context = null;
            DbContextOptionsBuilder<DataContext> options = null;
            if (contextType == "sqlite")
            {
                // Sqlite database
                options = new DbContextOptionsBuilder<DataContext>();
                options.UseSqlite(contextParams);
                context = new DataContext(options.Options);
                context.Database.EnsureCreated();
            }
            else if (contextType == "fake")
            {
                // Fake in-memory database for tests
                options = new DbContextOptionsBuilder<DataContext>();
                options.UseInMemoryDatabase(databaseName: contextParams);
                context = new DataContext(options.Options);
                context.Database.EnsureCreated();
            }
            return context;
        }
    }
}
