using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestWebApi.Controllers;
using TestWebApi.DataLayer;
using TestWebApi.DataLayer.DataObjects;
using TestWebApi.DataLayer.Result;
using TestWebApi.DataProvider;
using TestWebApi.DataProvider.DbObjects;
using TestWebApi.Repository;

namespace TestWebApi.ApiTests
{
    /// <summary>
    /// Tests for companies controller logic
    /// </summary>
    [TestClass]
    public class CompaniesControllerTests
    {
        private Guid applieId = Guid.NewGuid();
        private Guid britishAirwaysId = Guid.NewGuid();
        private Guid heinekenId = Guid.NewGuid();
        private Guid panasonicId = Guid.NewGuid();
        private Guid porscheId = Guid.NewGuid();

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            // It initializes in-memory database when the test class initializing
            DbContext dbContext = ContextBuilder.Build("fake", "test_db");
        }

        [TestInitialize]
        public void TestInitialize()
        {
            using (DbContext dbContext = ContextBuilder.Build("fake", "test_db"))
            {
                DbCompany apple = new DbCompany()
                {
                    Id = applieId,
                    Name = "Apple Inc.",
                    Exchange = "NASDAQ",
                    Ticker = "AAPL",
                    Isin = "US0378331005",
                    WebSite = "http://www.apple.com"
                };
                DbCompany britishAirways = new DbCompany()
                {
                    Id = britishAirwaysId,
                    Name = "British Airways Plc",
                    Exchange = "Pink Sheets",
                    Ticker = "BAIRY",
                    Isin = "US1104193065",
                    WebSite = null
                };
                DbCompany heineken = new DbCompany()
                {
                    Id = heinekenId,
                    Name = "Heineken NV",
                    Exchange = "Euronext Amsterdam",
                    Ticker = "HEIA",
                    Isin = "NL0000009165",
                    WebSite = null
                };
                DbCompany panasonic = new DbCompany()
                {
                    Id = panasonicId,
                    Name = "Panasonic Corp",
                    Exchange = "Tokyo Stock Exchange",
                    Ticker = "6752",
                    Isin = "JP3866800000",
                    WebSite = "http://www.panasonic.co.jp"
                };
                DbCompany porsche = new DbCompany()
                {
                    Id = porscheId,
                    Name = "Porsche Automobil",
                    Exchange = "Deutsche B�rse",
                    Ticker = "PAH3",
                    Isin = "DE000PAH0038",
                    WebSite = "https://www.porsche.com/"
                };
                dbContext.Set<DbCompany>().Add(britishAirways);
                dbContext.Set<DbCompany>().Add(heineken);
                dbContext.Set<DbCompany>().Add(porsche);
                dbContext.Set<DbCompany>().Add(apple);
                dbContext.Set<DbCompany>().Add(panasonic);
                
                dbContext.SaveChanges();
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            using (DbContext dbContext = ContextBuilder.Build("fake", "test_db"))
            {
                dbContext.Set<DbCompany>().RemoveRange(dbContext.Set<DbCompany>());
                dbContext.SaveChanges();
            }
        }

        private DbSet<DbCompany> MockException()
        {
            throw new InvalidOperationException();
        }

        /// <summary>
        /// Get full ordered list of companies.
        /// List contains 5 items
        /// Result: success
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetList_Success()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                JsonResult result = await controller.GetList() as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.Success, container.Result.MessageCode);
                // Check received data
                List<CompanyObject> dataList = container.List.ToList();
                Assert.AreEqual(5, dataList.Count);
                Assert.AreEqual("US0378331005", dataList[0].Isin);
                Assert.AreEqual("US1104193065", dataList[1].Isin);
                Assert.AreEqual("NL0000009165", dataList[2].Isin);
                Assert.AreEqual("JP3866800000", dataList[3].Isin);
                Assert.AreEqual("DE000PAH0038", dataList[4].Isin);
            }
        }

        /// <summary>
        /// DbContext is null.
        /// It may happen when either database is not initialized, 
        /// or database is not available,
        /// or database settings are not set or incorrect
        /// List is null
        /// Result: storage is not ready
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetList_NoContext_Failed()
        {
            CompanyRepository repository = new CompanyRepository(null);
            CompaniesController controller = new CompaniesController(repository);
            JsonResult result = await controller.GetList() as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
            Assert.AreEqual(MessageCodes.StorageNotReady, container.Result.MessageCode);
            Assert.AreEqual(
                "Storage is not ready. Make sure database is available and configured properly.", 
                container.Result.Message);
            Assert.IsNull(container.List);
        }

        /// <summary>
        /// Exception occured while company list request from a storage
        /// List is null
        /// Result: internal error
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetList_ContextException_Failed()
        {
            var mockContext = new Mock<DbContext>();
            mockContext.Setup(x => x.Set<DbCompany>()).Returns(MockException);
            CompanyRepository repository = new CompanyRepository(mockContext.Object);
            CompaniesController controller = new CompaniesController(repository);
            JsonResult result = await controller.GetList() as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
            Assert.AreEqual(MessageCodes.InternalError, container.Result.MessageCode);
            Assert.IsNotNull(container.Result.ExceptionTrace);
        }

        /// <summary>
        /// Get Panasonic item by id.
        /// List contains 1 item - Panasonic
        /// Result: success
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemById_Success()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                JsonResult result = await controller.GetItemById(panasonicId) as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.Success, container.Result.MessageCode);
                // Check received data
                List<CompanyObject> dataList = container.List.ToList();
                Assert.AreEqual(1, dataList.Count);
                Assert.AreEqual("Panasonic Corp", dataList[0].Name);
            }
        }

        /// <summary>
        /// DbContext is null.
        /// It may happen when either database is not initialized, 
        /// or database is not available,
        /// or database settings are not set or incorrect
        /// Result: storage is not ready
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemById_NoContext_Failed()
        {
            CompanyRepository repository = new CompanyRepository(null);
            CompaniesController controller = new CompaniesController(repository);
            JsonResult result = await controller.GetItemById(panasonicId) as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
            Assert.AreEqual(MessageCodes.StorageNotReady, container.Result.MessageCode);
            Assert.AreEqual(
                "Storage is not ready. Make sure database is available and configured properly.",
                container.Result.Message);
            Assert.IsNull(container.List);
        }

        /// <summary>
        /// Attempt to get a company which is not found in a storage.
        /// List is null
        /// Result: company is not found
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemById_NotFound_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                // Pass new guid to generate a case with absent Id
                JsonResult result = await controller.GetItemById(Guid.NewGuid()) as JsonResult;
                // Response code: Bad request
                Assert.AreEqual(400, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.CompanyNotFound, container.Result.MessageCode);
                Assert.AreEqual(
                    "Company is not found. It is likely removed.",
                    container.Result.Message);
                Assert.IsNull(container.List);
            }
        }

        /// <summary>
        /// Exception occured while company request from a storage
        /// List is null
        /// Result: internal error
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemById_ContextException_Failed()
        {
            var mockContext = new Mock<DbContext>();
            mockContext.Setup(x => x.Set<DbCompany>()).Returns(MockException);
            CompanyRepository repository = new CompanyRepository(mockContext.Object);
            CompaniesController controller = new CompaniesController(repository);
            JsonResult result = await controller.GetItemById(panasonicId) as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
            Assert.AreEqual(MessageCodes.InternalError, container.Result.MessageCode);
            Assert.IsNotNull(container.Result.ExceptionTrace);
        }

        /// <summary>
        /// Get Heineken item by ISIN.
        /// List contains 1 item - Heineken
        /// Result: success
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemByIsin_Success()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                JsonResult result = await controller.GetItemByIsin("nl0000009165") as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.Success, container.Result.MessageCode);
                // Check received data
                List<CompanyObject> dataList = container.List.ToList();
                Assert.AreEqual(1, dataList.Count);
                Assert.AreEqual("Heineken NV", dataList[0].Name);
                Assert.AreEqual("NL0000009165", dataList[0].Isin);
            }
        }

        /// <summary>
        /// DbContext is null.
        /// It may happen when either database is not initialized, 
        /// or database is not available,
        /// or database settings are not set or incorrect
        /// List is null
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemByIsin_NoContext_Failed()
        {
            CompanyRepository repository = new CompanyRepository(null);
            CompaniesController controller = new CompaniesController(repository);
            JsonResult result = await controller.GetItemByIsin("NL0000009165") as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
            Assert.AreEqual(MessageCodes.StorageNotReady, container.Result.MessageCode);
            Assert.AreEqual(
                "Storage is not ready. Make sure database is available and configured properly.",
                container.Result.Message);
            Assert.IsNull(container.List);
        }

        /// <summary>
        /// Attempt to get a company which is not found in a storage by ISIN.
        /// List is null
        /// Result: company is not found
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemByIsin_NotFound_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                // Pass new guid to generate a case with absent ISIN
                JsonResult result = await controller.GetItemByIsin("ERROR14587") as JsonResult;
                // Response code: Bad request
                Assert.AreEqual(400, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.CompanyNotFound, container.Result.MessageCode);
                Assert.AreEqual(
                    "Company with specified ISIN is not found. It is likely removed.",
                    container.Result.Message);
                Assert.IsNull(container.List);
            }
        }

        /// <summary>
        /// Exception occured while company request from a storage
        /// List is null
        /// Result: internal error
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task GetItemByIsin_ContextException_Failed()
        {
            var mockContext = new Mock<DbContext>();
            mockContext.Setup(x => x.Set<DbCompany>()).Returns(MockException);
            CompanyRepository repository = new CompanyRepository(mockContext.Object);
            CompaniesController controller = new CompaniesController(repository);
            JsonResult result = await controller.GetItemByIsin("US15676478") as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
            Assert.AreEqual(MessageCodes.InternalError, container.Result.MessageCode);
            Assert.IsNotNull(container.Result.ExceptionTrace);
        }

        /// <summary>
        /// Create GAZPROM company.
        /// Find the created company by its ISIN in other context.
        /// Result: success
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_Success()
        {
            Guid testId = Guid.NewGuid();
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = testId,
                    Name = "GAZPROM",
                    Exchange = "London Stock Exchange",
                    Isin = "Us3682872078",
                    StockTicker = "OGZD",
                    WebSite = "https://www.gazprom.com"
                };
                JsonResult result = await controller.CreateItem(company) as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.Success, msg.MessageCode);
            }
            // New context to check results
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                JsonResult result = await controller.GetItemByIsin("us3682872078") as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.Success, container.Result.MessageCode);
                // Check received data
                List<CompanyObject> dataList = container.List.ToList();
                Assert.AreEqual(1, dataList.Count);
                CompanyObject c = dataList[0];
                Assert.AreEqual("GAZPROM", c.Name);
                Assert.AreEqual("London Stock Exchange", c.Exchange);
                Assert.AreEqual("US3682872078", c.Isin);
                Assert.AreEqual("OGZD", c.StockTicker);
                Assert.AreEqual("https://www.gazprom.com", c.WebSite);
                // Check that Id is substituted with the new one, generated by server-side
                Assert.AreNotEqual(testId, c.Id);
            }
        }

        /// <summary>
        /// DbContext is null.
        /// It may happen when either database is not initialized, 
        /// or database is not available,
        /// or database settings are not set or incorrect
        /// Result: storage is not ready
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_NoContext_Failed()
        {
            Guid testId = Guid.NewGuid();
            String testIsin = "US3682872078";
            CompanyRepository repository = new CompanyRepository(null);
            CompaniesController controller = new CompaniesController(repository);
            CompanyObject company = new CompanyObject()
            {
                Id = testId,
                Name = "GAZPROM",
                Exchange = "London Stock Exchange",
                Isin = testIsin,
                StockTicker = "OGZD",
                WebSite = "https://www.gazprom.com"
            };
            JsonResult result = await controller.CreateItem(company) as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            ResultMessage msg = (ResultMessage)result.Value;
            Assert.AreEqual(MessageCodes.StorageNotReady, msg.MessageCode);
            Assert.AreEqual(
                "Storage is not ready. Make sure database is available and configured properly.",
                msg.Message);
        }

        /// <summary>
        /// Create a new company.
        /// There is already existing company with the same ISIN.
        /// Result: company with specified ISIN already exists
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_ExistingIsin_Failed()
        {
            Guid testId = Guid.NewGuid();
            String testIsin = "NL0000009165";
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = testId,
                    Name = "GAZPROM",
                    Exchange = "London Stock Exchange",
                    Isin = testIsin,
                    StockTicker = "OGZD"
                };
                JsonResult result = await controller.CreateItem(company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyIsinExists, msg.MessageCode);
            }
        }

        /// <summary>
        /// Create a new company.
        /// Name is not set.
        /// Result: company name is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_NameIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = Guid.Empty,
                    Name = "",
                    Exchange = "London Stock Exchange",
                    Isin = "US3682872078",
                    StockTicker = "OGZD"
                };
                JsonResult result = await controller.CreateItem(company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyNameNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Create a new company.
        /// ISIN is not set.
        /// Result: company ISIN is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_IsinIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = Guid.Empty,
                    Name = "GAZPROM",
                    Exchange = "London Stock Exchange",
                    Isin = "",
                    StockTicker = "OGZD"
                };
                JsonResult result = await controller.CreateItem(company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyIsinNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Create a new company.
        /// Exchange Name is not set.
        /// Result: company exchange is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_ExchangeIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = Guid.Empty,
                    Name = "GAZPROM",
                    Exchange = null,
                    Isin = "US3682872078",
                    StockTicker = "OGZD"
                };
                JsonResult result = await controller.CreateItem(company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyExchangeNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Create a new company.
        /// Ticker is not set.
        /// Result: company ticker is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_TickerIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = Guid.Empty,
                    Name = "GAZPROM",
                    Exchange = "London Stock Exchange",
                    Isin = "US3682872078",
                    StockTicker = ""
                };
                JsonResult result = await controller.CreateItem(company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyTickerNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Create a new company.
        /// ISIN is set but it is not valid
        /// Result: company name is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_InvalidIsin_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = Guid.Empty,
                    Name = "GAZPROM",
                    Exchange = "London Stock Exchange",
                    Isin = "A123478569",
                    StockTicker = "OGZD"
                };
                JsonResult result = await controller.CreateItem(company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyIsinInvalid, msg.MessageCode);
            }
        }

        /// <summary>
        /// Create GAZPROM company.
        /// Exception occured while saving process.
        /// Result: internal error
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task CreateCompany_SavingException_Failed()
        {
            Guid testId = Guid.NewGuid();
            String testIsin = "US3682872078";
            var mockContext = new Mock<DbContext>();
            mockContext.Setup(x => x.Set<DbCompany>()).Returns(MockException);
            CompanyRepository repository = new CompanyRepository(mockContext.Object);
            CompaniesController controller = new CompaniesController(repository);
            CompanyObject company = new CompanyObject()
            {
                Id = testId,
                Name = "GAZPROM",
                Exchange = "London Stock Exchange",
                Isin = testIsin,
                StockTicker = "OGZD",
                WebSite = "https://www.gazprom.com"
            };
            JsonResult result = await controller.CreateItem(company) as JsonResult;
            // Response code: OK
            Assert.AreEqual(400, result.StatusCode);
            ResultMessage msg = (ResultMessage)result.Value;
            Assert.AreEqual(MessageCodes.InternalError, msg.MessageCode);
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// Find the updated company by its Id in other context.
        /// Result: success
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_Success()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = panasonicId,
                    Name = "Alphabet",
                    Exchange = "NASDAQ",
                    Isin = "us02079k1079",
                    StockTicker = "GOOG",
                    WebSite = "https://www.google.com"
                };
                JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.Success, msg.MessageCode);
            }
            // New context to check results
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                JsonResult result = await controller.GetItemById(panasonicId) as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.Success, container.Result.MessageCode);
                // Check received data
                List<CompanyObject> dataList = container.List.ToList();
                Assert.AreEqual(1, dataList.Count);
                CompanyObject c = dataList[0];
                Assert.AreEqual("Alphabet", c.Name);
                Assert.AreEqual("NASDAQ", c.Exchange);
                Assert.AreEqual("US02079K1079", c.Isin);
                Assert.AreEqual("GOOG", c.StockTicker);
                Assert.AreEqual("https://www.google.com", c.WebSite);
                Assert.AreEqual(panasonicId, c.Id);
            }
        }

        /// <summary>
        /// DbContext is null.
        /// It may happen when either database is not initialized, 
        /// or database is not available,
        /// or database settings are not set or incorrect
        /// Result: storage is not ready
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_NoContext_Failed()
        {
            CompanyRepository repository = new CompanyRepository(null);
            CompaniesController controller = new CompaniesController(repository);
            CompanyObject company = new CompanyObject()
            {
                Id = panasonicId,
                Name = "Alphabet",
                Exchange = "NASDAQ",
                Isin = "US02079K1079",
                StockTicker = "GOOG",
                WebSite = "https://www.google.com"
            };
            JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
            // Response code: Bad request
            Assert.AreEqual(400, result.StatusCode);
            ResultMessage msg = (ResultMessage)result.Value;
            Assert.AreEqual(MessageCodes.StorageNotReady, msg.MessageCode);
            Assert.AreEqual(
                "Storage is not ready. Make sure database is available and configured properly.",
                msg.Message);
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// There is already existing company with the same ISIN.
        /// Result: company with specified ISIN already exists
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_ExistingIsin_Failed()
        {
            String testIsin = "NL0000009165";
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = panasonicId,
                    Name = "Alphabet",
                    Exchange = "NASDAQ",
                    Isin = testIsin,
                    StockTicker = "GOOG",
                    WebSite = "https://www.google.com"
                };
                JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyIsinExists, msg.MessageCode);
            }
            // New context to check that company was not changed
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                JsonResult result = await controller.GetItemById(panasonicId) as JsonResult;
                // Response code: OK
                Assert.AreEqual(200, result.StatusCode);
                DataList<CompanyObject> container = (DataList<CompanyObject>)result.Value;
                Assert.AreEqual(MessageCodes.Success, container.Result.MessageCode);
                // Check received data
                List<CompanyObject> dataList = container.List.ToList();
                Assert.AreEqual(1, dataList.Count);
                CompanyObject c = dataList[0];
                Assert.AreEqual("Panasonic Corp", c.Name);
                Assert.AreEqual(panasonicId, c.Id);
            }
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// Name is not set.
        /// Result: company name is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_NameIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = panasonicId,
                    Name = null,
                    Exchange = "NASDAQ",
                    Isin = "US02079K1079",
                    StockTicker = "GOOG",
                    WebSite = "https://www.google.com"
                };
                JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyNameNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// ISIN is not set.
        /// Result: company ISIN is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_IsinIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = panasonicId,
                    Name = "Alphabet",
                    Exchange = "NASDAQ",
                    StockTicker = "GOOG",
                    WebSite = "https://www.google.com"
                };
                JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyIsinNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// Exchange Name is not set.
        /// Result: company exchange is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_ExchangeIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = panasonicId,
                    Name = "Alphabet",
                    Isin = "US02079K1079",
                    StockTicker = "GOOG",
                    WebSite = "https://www.google.com"
                };
                JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyExchangeNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// Ticker is not set.
        /// Result: company ticker is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_TickerIsNotSet_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = panasonicId,
                    Name = "Alphabet",
                    Exchange = "NASDAQ",
                    Isin = "US02079K1079",
                    StockTicker = "",
                    WebSite = "https://www.google.com"
                };
                JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyTickerNotSet, msg.MessageCode);
            }
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// ISIN is set but it is not valid
        /// Result: company name is not set
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_InvalidIsin_Failed()
        {
            using (DbContext context = ContextBuilder.Build("fake", "test_db"))
            {
                CompanyRepository repository = new CompanyRepository(context);
                CompaniesController controller = new CompaniesController(repository);
                CompanyObject company = new CompanyObject()
                {
                    Id = panasonicId,
                    Name = "Alphabet",
                    Exchange = "NASDAQ",
                    Isin = "A",
                    StockTicker = "GOOG",
                    WebSite = "https://www.google.com"
                };
                JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
                // Response code: bad request
                Assert.AreEqual(400, result.StatusCode);
                ResultMessage msg = (ResultMessage)result.Value;
                Assert.AreEqual(MessageCodes.CompanyIsinInvalid, msg.MessageCode);
            }
        }

        /// <summary>
        /// Change Panasonic to Google.
        /// Exception occured while updating process.
        /// Result: internal error
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task UpdateCompany_SavingException_Failed()
        {
            var mockContext = new Mock<DbContext>();
            mockContext.Setup(x => x.Set<DbCompany>()).Returns(MockException);
            CompanyRepository repository = new CompanyRepository(mockContext.Object);
            CompaniesController controller = new CompaniesController(repository);
            CompanyObject company = new CompanyObject()
            {
                Id = panasonicId,
                Name = "Alphabet",
                Exchange = "NASDAQ",
                Isin = "US02079K1079",
                StockTicker = "GOOG",
                WebSite = "https://www.google.com"
            };
            JsonResult result = await controller.UpdateItem(panasonicId, company) as JsonResult;
            // Response code: OK
            Assert.AreEqual(400, result.StatusCode);
            ResultMessage msg = (ResultMessage)result.Value;
            Assert.AreEqual(MessageCodes.InternalError, msg.MessageCode);
        }
    }
}
