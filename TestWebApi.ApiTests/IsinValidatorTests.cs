using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestWebApi.Controllers;
using TestWebApi.DataLayer;
using TestWebApi.DataLayer.DataObjects;
using TestWebApi.DataLayer.Result;
using TestWebApi.DataProvider;
using TestWebApi.DataProvider.DbObjects;
using TestWebApi.Repository;

namespace TestWebApi.ApiTests
{
    /// <summary>
    /// Tests for ISIN validator
    /// </summary>
    [TestClass]
    public class IsinValidatorTests
    {
        /// <summary>
        /// Validate passed ISIN
        /// Result: success
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void IsinValidate_Success()
        {
            Boolean valid = IsinValidator.Validate("NL0000009165");
            Assert.IsTrue(valid);
        }

        /// <summary>
        /// Validate passed ISIN
        /// Result: failed
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void IsinValidate_Failed()
        {
            Boolean valid = IsinValidator.Validate("N");
            Assert.IsFalse(valid);
            valid = IsinValidator.Validate("000000009165");
            Assert.IsFalse(valid);
            valid = IsinValidator.Validate("");
            Assert.IsFalse(valid);
            valid = IsinValidator.Validate("N10000009165");
            Assert.IsFalse(valid);
            valid = IsinValidator.Validate("NL-000009165");
            Assert.IsFalse(valid);
        }
    }
}
